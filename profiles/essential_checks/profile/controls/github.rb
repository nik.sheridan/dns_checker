
dns_targets = yaml(content: inspec.profile.file('services.yml')).params['dns']

dns_targets.each do |d|
  describe host(d['service_name'], port: d['port'], protocol: 'tcp') do
    it { should be_resolvable }
    it { should be_reachable }
  end
end

