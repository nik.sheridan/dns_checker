FROM ruby:2.7
MAINTAINER Mike Stein
WORKDIR /app
ADD ./app /app/
RUN set -uex; \
    bundle install; \
    adduser rubyapp; 
USER rubyapp
CMD [ "ruby", "main.rb" ]
