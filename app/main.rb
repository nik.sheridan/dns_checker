require './libs/inspec_helpers'
require './libs/inspec_runner'
require './libs/prometheus_client'


inspec_runs,inspec_status = boot_prometheus()

while true do
  inspec_runs.increment
  puts "Running Inspec"
  status = run_inspec()
  inspec_status.set status ? 1 : 0
 sleep(5)
end

