require 'prometheus_exporter'
require 'prometheus_exporter/server'
# client allows instrumentation to send info to server


# start and seed prometheus metrics
def boot_prometheus
  PrometheusExporter
  server = PrometheusExporter::Server::WebServer.new bind: '0.0.0.0', port: 12345, verbose: true
  server.start

  PrometheusExporter::Client.default = PrometheusExporter::LocalClient.new(collector: server.collector)

  inspec_runs = PrometheusExporter::Metric::Counter.new("inspec_runs", 'A count of inspec runs')
  inspec_status = PrometheusExporter::Metric::Gauge.new("inspec_status", 'Inspec test status')
  server.collector.register_metric(inspec_runs)
  server.collector.register_metric(inspec_status)
  return inspec_runs,inspec_status
end



