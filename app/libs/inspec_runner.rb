require 'inspec'
def run_inspec
  r = Inspec::Runner.new()
  r.add_target('./profiles/essential_checks')
  r.run
  print_analyser r.report
end
