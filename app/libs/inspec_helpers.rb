# takes an inspec report and returns status
def print_analyser(report)
  passed = true
  report[:controls].each do |control|
    case  control[:status]
    when 'passed'
      passed &= true
    when 'failed'
      passed &= false
    end
  end
  passed
end
