# Novice Guide

Checking it out, outside a container, and you want to pull it apart.  On Ubuntu 20.04.

```bash
sudo snap install ruby 
sudo apt install ruby-bundler
sudo apt install ruby-dev
bundle install
```

Check out the stuff from prometheus:

```bash 
curl 127.0.0.1:12345/metrics
```

dont browse to http://localhost:12345

```bash
Not Found! The Prometheus Ruby Exporter only listens on /metrics and /send-metrics
```